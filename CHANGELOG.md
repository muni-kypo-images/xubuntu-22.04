# Changelog

## [vbox-0.1.1] - 2023-10-03
### Added
- First version for VBox

## [qemu-0.1.1] - 2023-09-25
### Fixed
- Bug when apt would fail after adding an apt repository, see [issue](https://gitlab.ics.muni.cz/muni-kypo-images/xubuntu-22.04/-/issues/1).

## [qemu-0.1.0] - 2023-09-21
### Added
- First version


[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/xubuntu-22.04/-/tree/qemu-0.1.0
[qemu-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/xubuntu-22.04/-/tree/qemu-0.1.1
[vbox-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/xubuntu-22.04/-/tree/vbox-0.1.1
